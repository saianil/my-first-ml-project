#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Author = Anil Hhmam


import pandas as pd

# Reading the Dataframe
df_path = "/Users/anilhimam/Downloads/Coastal Data System - Waves (Mooloolaba) 01-2017 to 06 - 2019.csv"
df_data = pd.read_csv(df_path)

# Selecting the labels and the features
df_features = ['Hs', 'Tz', 'Tp', 'Peak Direction', 'SST']

X = df_data[df_features]
y = df_data.Hmax

# -------------------------------------------------------------------------------------------------------


from sklearn.model_selection import train_test_split

X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.1)

# -------------------------------------------------------------------------------------------------------

'''
print(len(X_train))
print(len(X_valid))
print(len(y_valid))
print(len(y_train))

def trouble_shoot_approch_1():

    from sklearn.ensemble import RandomForestRegressor
    from sklearn.metrics import mean_absolute_error
    
    model = RandomForestRegressor(n_estimators = 20)
    model.fit(X_train, y_train)
    
    predictions = model.predict(X_valid)
    print(mean_absolute_error(predictions, y_valid))
    '''


# -------------------------------------------------------------------------------------------------------


def Imputing_Data():
    # To impute the dataset of rows containing empty values with mean value

    from sklearn.impute import SimpleImputer

    my_imputer = SimpleImputer()

    global imputed_X_train
    global imputed_X_valid

    imputed_X_train = pd.DataFrame(my_imputer.fit_transform(X_train))
    imputed_X_valid = pd.DataFrame(my_imputer.transform(X_valid))

    # After imputing the values the names of the cols have to be re added as they were erased
    imputed_X_train.columns = X_train.columns
    imputed_X_valid.columns = X_valid.columns


Imputing_Data()
# -------------------------------------------------------------------------------------------------------

from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from joblib import load, dump


def model_training():
    model = RandomForestRegressor(n_estimators=40, random_state=1)
    model.fit(imputed_X_train, y_train)

    # Predictions
    predictions = model.predict(imputed_X_valid)
    print(mean_absolute_error(predictions, y_valid))

    inp = input("Enter yes or no to save the model for the expeted performance:")

    if inp == "yes" or inp == "Yes" or inp == "YES":
        dump(model, "waves_random_forest.joblib")

    else:
        pass


# -------------------------------------------------------------------------------------------------------


def model_testing():
    data = []

    for i in range(5):
        inp = float(input("Enter the data to be entered into the list for the prediction:"))
        data.append(inp)

    model = load("waves_random_forest.joblib")
    prediction = model.predict([data])

    print("\nThe prediction of the maximum height of the wave is:\n", prediction)

# 1.078		4.285	5.18	40	26.4
# 1.86
model_training()
#model_testing()
